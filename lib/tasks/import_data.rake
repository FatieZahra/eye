require 'csv'

task :import_occupation => :environment do 
	csv_text = File.read('/Users/fatimahsanni/Desktop/occupation1.csv')
	csv = CSV.parse(csv_text, :headers => true)
	csv.each do |row|
		Occupation.create!(row.to_hash)
	end
end
 
task :import_state => :environment do 
	csv_text = File.read('/Users/fatimahsanni/Downloads/states.csv')
	csv = CSV.parse(csv_text, :headers => true)
	csv.each do |row|
		State.create!(row.to_hash)
	end
end
   
task :import_nationality => :environment do 
	csv_text = File.read('/Users/fatimahsanni/Desktop/nationalities.csv')
	csv = CSV.parse(csv_text, :headers => true)
	csv.each do |row|
		Nationality.create!(row.to_hash)
	end
end