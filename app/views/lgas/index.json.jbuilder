json.array!(@lgas) do |lga|
  json.extract! lga, :id, :name
  json.url lga_url(lga, format: :json)
end
