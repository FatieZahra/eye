json.array!(@patients) do |patient|
  json.extract! patient, :id, :surname, :first_name, :middle_name, :gender, :dob, :address, :phone, :email, :next_of_kin, :next_of_kin_phone, :consultant_id, :clinic_id
  json.url patient_url(patient, format: :json)
end
