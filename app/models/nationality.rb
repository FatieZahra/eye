class Nationality < ActiveRecord::Base
	has_many :patients
	validates :name, presence: true
end
