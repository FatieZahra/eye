class Clinic < ActiveRecord::Base
	has_many :consultants
	has_many :patients, through: :consultants
	validates :name, presence: true
end
