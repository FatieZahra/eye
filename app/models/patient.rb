class Patient < ActiveRecord::Base
	after_create :set_mrn
  belongs_to :consultant
  belongs_to :clinic
  validates :surname, :first_name, :phone, :address, :gender, :next_of_kin, :next_of_kin_phone, :consultant, :occupation, :state, :religion, presence: true
  belongs_to :occupation
  belongs_to :nationality
  belongs_to :religion
  belongs_to :state
  belongs_to :lga
   Gender = %w(Male Female)

   def set_mrn
   	update_attribute(:mrn, "P#{self.id}H")
   end
end
