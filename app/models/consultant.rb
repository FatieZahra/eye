class Consultant < ActiveRecord::Base
  belongs_to :clinic
  has_many :patients
  validates :name, :clinic, presence: true
end
