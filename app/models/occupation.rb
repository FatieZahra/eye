class Occupation < ActiveRecord::Base
	validates :name, presence: true
end
