class State < ActiveRecord::Base
	has_many :patients
	has_many :lgas
	validates :name, presence: true
end
