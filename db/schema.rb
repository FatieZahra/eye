# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150501025514) do

  create_table "clinics", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "consultants", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "clinic_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "consultants", ["clinic_id"], name: "index_consultants_on_clinic_id", using: :btree

  create_table "lgas", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "state_id",   limit: 4
  end

  add_index "lgas", ["state_id"], name: "index_lgas_on_state_id", using: :btree

  create_table "nationalities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "occupations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "patients", force: :cascade do |t|
    t.string   "surname",           limit: 255
    t.string   "first_name",        limit: 255
    t.string   "middle_name",       limit: 255
    t.string   "gender",            limit: 255
    t.date     "dob"
    t.string   "address",           limit: 255
    t.string   "phone",             limit: 255
    t.string   "email",             limit: 255
    t.string   "next_of_kin",       limit: 255
    t.string   "next_of_kin_phone", limit: 255
    t.integer  "consultant_id",     limit: 4
    t.integer  "clinic_id",         limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "mrn",               limit: 255
    t.boolean  "registered",        limit: 1,   default: false, null: false
    t.integer  "religion_id",       limit: 4
    t.integer  "occupation_id",     limit: 4
    t.integer  "nationality_id",    limit: 4
    t.integer  "state_id",          limit: 4
    t.integer  "lga_id",            limit: 4
  end

  add_index "patients", ["clinic_id"], name: "index_patients_on_clinic_id", using: :btree
  add_index "patients", ["consultant_id"], name: "index_patients_on_consultant_id", using: :btree
  add_index "patients", ["lga_id"], name: "index_patients_on_lga_id", using: :btree
  add_index "patients", ["nationality_id"], name: "index_patients_on_nationality_id", using: :btree
  add_index "patients", ["occupation_id"], name: "index_patients_on_occupation_id", using: :btree
  add_index "patients", ["religion_id"], name: "index_patients_on_religion_id", using: :btree
  add_index "patients", ["state_id"], name: "index_patients_on_state_id", using: :btree

  create_table "religions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "consultants", "clinics"
  add_foreign_key "lgas", "states"
  add_foreign_key "patients", "clinics"
  add_foreign_key "patients", "consultants"
  add_foreign_key "patients", "lgas"
  add_foreign_key "patients", "nationalities"
  add_foreign_key "patients", "religions"
  add_foreign_key "patients", "states"
end
