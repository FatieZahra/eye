class AddLgaToPatients < ActiveRecord::Migration
  def change
    add_reference :patients, :lga, index: true
    add_foreign_key :patients, :lgas
  end
end
