class AddStateToPatients < ActiveRecord::Migration
  def change
    add_reference :patients, :state, index: true
    add_foreign_key :patients, :states
  end
end
