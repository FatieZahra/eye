class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :surname
      t.string :first_name
      t.string :middle_name
      t.string :gender
      t.date :dob
      t.string :address
      t.string :phone
      t.string :email
      t.string :next_of_kin
      t.string :next_of_kin_phone
      t.belongs_to :consultant, index: true
      t.belongs_to :clinic, index: true

      t.timestamps null: false
    end
    add_foreign_key :patients, :consultants
    add_foreign_key :patients, :clinics
  end
end
