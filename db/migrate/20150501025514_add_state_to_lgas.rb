class AddStateToLgas < ActiveRecord::Migration
  def change
    add_reference :lgas, :state, index: true
    add_foreign_key :lgas, :states
  end
end
