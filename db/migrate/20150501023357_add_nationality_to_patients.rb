class AddNationalityToPatients < ActiveRecord::Migration
  def change
    add_reference :patients, :nationality, index: true
    add_foreign_key :patients, :nationalities
  end
end
