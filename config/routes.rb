Rails.application.routes.draw do

  resources :occupations

  resources :lgas

  resources :states

  resources :nationalities

  resources :religions

  resources :patients

  resources :consultants

  resources :clinics

  root "patients#index"

end
